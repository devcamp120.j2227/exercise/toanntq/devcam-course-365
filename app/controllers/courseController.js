// Import thư viện Mongoose
const mongoose = require("mongoose");

// Import Module Course Model
const courseModel = require("../models/courseModel");

const getAllCourse = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    courseModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get all course successfully",
            data: data
        })
    })
}

const createCourse = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;

    // {
    // courseCode: String, unique, required
    // courseName: String, required
    // price: Number, required
    // discountPrice: Number, required,
    // duration: String, required,
    // level: String, required,
    // coverImage: String, required,
    // teacherName: String, required,
    // teacherPhoto: String, required,
    // isPopular: Boolean, default: true,
    // isTrending: Boolean, default: false,
    // }

    // B2: Validate dữ liệu
    // Kiểm tra courseCode có hợp lệ hay không
    if (!body.courseCode) {
        return response.status(400).json({
            status: "Bad Request",
            message: "courseCode không hợp lệ"
        })
    }

    // Kiểm tra courseName có hợp lệ hay không
    if (!body.courseName) {
        return response.status(400).json({
            status: "Bad Request",
            message: "courseName không hợp lệ"
        })
    }

    // Kiểm tra price có hợp lệ hay không
    if (isNaN(body.price) || body.price < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Price không hợp lệ"
        })
    }

    // Kiểm tra discountPrice có hợp lệ hay không
    if (isNaN(body.discountPrice) || body.discountPrice < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "discountPrice không hợp lệ"
        })
    }

    // Kiểm tra duration có hợp lệ hay không
    if (!body.duration) {
        return response.status(400).json({
            status: "Bad Request",
            message: "duration không hợp lệ"
        })
    }

    // Kiểm tra level có hợp lệ hay không
    if (!body.level) {
        return response.status(400).json({
            status: "Bad Request",
            message: "level không hợp lệ"
        })
    }

    // Kiểm tra coverImage có hợp lệ hay không
    if (!body.coverImage) {
        return response.status(400).json({
            status: "Bad Request",
            message: "coverImage không hợp lệ"
        })
    }

    // Kiểm tra teacherName có hợp lệ hay không
    if (!body.teacherName) {
        return response.status(400).json({
            status: "Bad Request",
            message: "teacherName không hợp lệ"
        })
    }

    // Kiểm tra teacherPhoto có hợp lệ hay không
    if (!body.teacherPhoto) {
        return response.status(400).json({
            status: "Bad Request",
            message: "teacherPhoto không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const newCourse = {
        _id: mongoose.Types.ObjectId(),
        courseCode: body.courseCode,
        courseName: body.courseName,
        price: body.price,
        discountPrice: body.discountPrice,
        duration: body.duration,
        level: body.level,
        coverImage: body.coverImage,
        teacherName: body.teacherName,
        teacherPhoto: body.teacherPhoto,
        isPopular: true,
        isTrending: false
    }

    courseModel.create(newCourse, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(201).json({
            status: "Create course successfully",
            data: data
        })
    })
}

const getCourseById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    courseModel.findById(courseId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail course successfully",
            data: data
        })
    })
}

const updateCoureById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;
    const body = request.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    if (body.courseCode !== undefined && body.courseCode.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "courseCode không hợp lệ"
        })
    }

    if (body.price !== undefined && (isNaN(body.price) || body.price < 0)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "No Student không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    const updateCourse = {}

    if (body.courseCode !== undefined) {
        updateCourse.courseCode = body.courseCode
    }

    if (body.courseName !== undefined) {
        updateCourse.courseName = body.courseName
    }

    if (body.price !== undefined) {
        updateCourse.price = body.price
    }

    courseModel.findByIdAndUpdate(courseId, updateCourse, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update course successfully",
            data: data
        })
    })
}

const deleteCourseById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const courseId = request.params.courseId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete course successfully"
        })
    })
}

module.exports = {
    getAllCourse,
    createCourse,
    getCourseById,
    updateCoureById,
    deleteCourseById
}