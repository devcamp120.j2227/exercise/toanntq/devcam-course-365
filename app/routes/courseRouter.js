// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import course middleware
const courseMiddleware = require("../middlewares/courseMiddleware");

// Import course controller
const courseController = require("../controllers/courseController")

router.get("/courses", courseMiddleware.getAllCourseMiddleware, courseController.getAllCourse);

router.post("/courses", courseMiddleware.createCourseMiddleware, courseController.createCourse);

router.get("/courses/:courseId", courseMiddleware.getDetailCourseMiddleware, courseController.getCourseById)

router.put("/courses/:courseId", courseMiddleware.updateCourseMiddleware, courseController.updateCoureById)

router.delete("/courses/:courseId", courseMiddleware.deleteCourseMiddleware, courseController.deleteCourseById)

module.exports = router;